#include <stdio.h>

struct Person {
    unsigned char id;
    unsigned char age;
};

/*struct Person createPerson(unsigned char id, unsigned char age) {
    struct Person p;
    p.id = id;
    p.age = age;
    return p;
}*/

unsigned int hashCode(struct Person *p) {
    int id = p->id;
    int age = p->age;
    return id + age;
}

char compare(struct Person *p1, struct Person *p2) {
    unsigned int p1Hash = hashCode(p1);
    unsigned int p2Hash = hashCode(p2);
    if (p1Hash < p2Hash)
        return -1;
    if (p1Hash > p2Hash)
        return 1;
    return 0;
}

int part(struct Person a[], int l, int u) {
    struct Person v;
    int i, j;
    v = a[l];
    i = l;
    j = u + 1;
    do {
        do {
            i++;
        } while (compare(&a[i], &v) == -1 && i <= u);
        do {
            j--;
        } while (compare(&v, &a[j]) == -1);
        if (i < j) {
            struct Person tp = a[i];
            a[i] = a[j];
            a[j] = tp;
        }
    } while (i < j);
    a[l] = a[j];
    a[j] = v;
    return j;
}

void qsort(struct Person a[], int l, int u) {
    int j;
    if (l < u) {
        j = part(a, l, u);
        qsort(a, l, j - 1);
        qsort(a, j + 1, u);
    }
}

int main() {

    struct Person persons[] = {
            {0, 25},
            {1, 22},
            {2, 23},
            {3, 24},
            {4, 26},
    };

    int n = 5;

    qsort(persons, 0, n - 1);

    for (int i = 0; i < 5; i++) {
        printf("Age %d - Id %d \n", persons[i].age, persons[i].id);
    }

    return 0;
}

//Used https://www.thecrazyprogrammer.com/2014/02/what-is-quick-sort-algorithm-and-c-program-to-implement-quick-sort.html

/*int partition(int a[], int l, int u) {
    int v, i, j, temp;
    v = a[l];
    i = l;
    j = u + 1;
    do {
        do
            i++;
        while (a[i] < v && i <= u);
        do
            j--;
        while (v < a[j]);
        if (i < j) {
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
    } while (i < j);
    a[l] = a[j];
    a[j] = v;
    return (j);
}

void quick_sort(int a[], int l, int u) {
    int j;
    if (l < u) {
        j = partition(a, l, u);
        quick_sort(a, l, j - 1);
        quick_sort(a, j + 1, u);
    }
}*/
